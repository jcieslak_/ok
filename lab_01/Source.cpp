#include <iostream>
#include "Point.h"
using namespace std;

int main() {
	
	Point my_point;
	my_point.setX(5.5);
	cout << "rozmiar obiektu my_point wynosi " << sizeof(my_point) << endl;
	cout << "wspolrzedna x obiektu my_point " << my_point.getX() << endl;
	cout << "wspolrzedna y obiektu my_point " << my_point.getY() << endl;


	float tmp;
	cout << "podaj wspolrzedna x \n";
	cin >> tmp;
	my_point.setX(tmp);
	cout << "podaj wspolrzedna y \n";
	cin >> tmp;
	my_point.setY(tmp);

	cout << "odleglosc my_point od (0,0) to " << my_point.distXY() << endl;
	cout << "\n ----------------------------------------------\n";
	Point new_point(1.22323, 1.3232);
	cout << "wspolrzedna x obiektu my_point " << new_point.getX() << endl;
	cout << "wspolrzedna y obiektu my_point " << new_point.getY() << endl;
	cout << "odleglosc new_point od (0,0) to " << new_point.distXY() << endl;
	float tmpX;
	cout << "podaj wspolrzedna x \n";
	cin >> tmpX;

	float tmpY;
	cout << "podaj wspolrzedna y \n";
	cin >> tmpY;

	Point newer_point(tmpX, tmpY);

	cout << "odleglosc new_point od (0,0) to " << newer_point.distXY() << endl;
	// albo
	cout << "drugi sposob" << endl;
	cout << "odleglosc new_point od (0,0) to " << Point(tmpX, tmpY).distXY() << endl;
	cout << "\n--------------------------------------------\n";
	
	cout << Point(1,1,2).distXY() << endl;

	system("pause");
	return 0;
}