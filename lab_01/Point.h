#pragma once
class Point {
	float x;
	float y;
public:
	Point(); //konstruktor
	Point(float, float);
	Point(float, float, int);
	void setX(float);
	void setY(float);
	float distXY();
	float getX();
	float getY();
};