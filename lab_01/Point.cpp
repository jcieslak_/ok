#pragma once
#include<math.h>
#include<iostream>
#include "Point.h"
using namespace std;
Point::Point() {
	cout << "Dziala konstruktor klasy Point bez arg. \n";
	x = 0.0;
	y = 0.0;
	
};
Point::Point(float a, float b): x(a), y(b) {//lista inicjalizacyjna
	cout << "Dziala konstruktor bazy Point z arg. \n";
};
Point::Point(float a, float b, int c) {
	float TEMPX;
	float TEMPY;
	float TEMPC;
	switch (c) {
	case 1:
		if (a >= 0 && b >= 0) {
			x = a;
			y = b;
			break;
		}
		else {
			cout << "Niepoprawne wartosci, cwiartki musza sie zgadzac z wspolrzednymi"<<endl;
			cout << "Prosz� poda� warto�� x jeszcze raz" << endl;
			cin >> TEMPX;
			cout << "Prosz� poda� warto�� y jeszcze raz" << endl;
			cin >> TEMPY;
			cout << "Prosz� poda� �wiartk� jeszcze raz" << endl;
			cin >> TEMPC;
			Point p = Point(TEMPX, TEMPY, TEMPC);
			break;
		}
	case 2:
		if (a <= 0 && b >= 0) {
			x = a;
			y = b;
			break;
		}
		else {
			cout << "Niepoprawne wartosci, cwiartki musza sie zgadzac z wspolrzednymi" << endl;
			cout << "Prosz� poda� warto�� x jeszcze raz" << endl;
			cin >> TEMPX;
			cout << "Prosz� poda� warto�� y jeszcze raz" << endl;
			cin >> TEMPY;
			cout << "Prosz� poda� �wiartk� jeszcze raz" << endl;
			cin >> TEMPC;
			Point p = Point(TEMPX, TEMPY, TEMPC);
			break;
		}
	case 3:
		if (a <= 0 && b <= 0) {
			x = a;
			y = b;
			break;
		}
		else {
			cout << "Niepoprawne wartosci, cwiartki musza sie zgadzac z wspolrzednymi" << endl;
			cout << "Prosz� poda� warto�� x jeszcze raz" << endl;
			cin >> TEMPX;
			cout << "Prosz� poda� warto�� y jeszcze raz" << endl;
			cin >> TEMPY;
			cout << "Prosz� poda� �wiartk� jeszcze raz" << endl;
			cin >> TEMPC;
			Point p = Point(TEMPX, TEMPY, TEMPC);
			break;
		}
	case 4:
		if (a <= 0 && b >= 0) {
			x = a;
			y = b;
			break;
		}
		else {
			cout << "Niepoprawne wartosci, cwiartki musza sie zgadzac z wspolrzednymi" << endl;
			cout << "Prosz� poda� warto�� x jeszcze raz" << endl;
			cin >> TEMPX;
			cout << "Prosz� poda� warto�� y jeszcze raz" << endl;
			cin >> TEMPY;
			cout << "Prosz� poda� �wiartk� jeszcze raz" << endl;
			cin >> TEMPC;
			Point p = Point(TEMPX, TEMPY, TEMPC);
			break;
		}
	default:
		cout << "Niepoprawna wartosc cwiartki prosze wybrac 1 2 3 lub 4 " << endl;
		cout << "Prosz� poda� warto�� x jeszcze raz" << endl;
		cin >> TEMPX;
		cout << "Prosz� poda� warto�� y jeszcze raz" << endl;
		cin >> TEMPY;
		cout << "Prosz� poda� �wiartk� jeszcze raz" << endl;
		cin >> TEMPC;
		Point p = Point(TEMPX, TEMPY, TEMPC);
		break;
	}
};
void Point::setX(float a) { x = a; };
void Point::setY(float b) { y = b; };
float Point::getX() { return x; };
float Point::getY() { return y; };
float Point::distXY() { return sqrtf(x*x + y*y); };