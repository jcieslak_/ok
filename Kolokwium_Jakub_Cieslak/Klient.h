#pragma once
#include<iostream>
#include"Adres.h"
using namespace std;

class Klient {
    string Name;
    string Surname;
    Adres adres;
 public:
     Klient();
     Klient(string Name, string Surname, Adres adres);

     void setName(string);
     void setSurname(string);
     void setAdres(Adres);

     string getName();
     string getSurname();
     Adres getAdres();
};