#pragma once
#include<iostream>
#include"Adres.h"
using namespace std;

class Bank {
	string Name;
	Adres adres;
public:
	Bank();
	Bank(string Name, Adres adres);

	void setName(string);
	void setAdres(Adres);

	Adres getAdres();
	string getName();
};
