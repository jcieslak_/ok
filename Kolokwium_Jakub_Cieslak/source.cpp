#include<iostream>
#include<string>
#include "Rachunek.h"


using namespace std;

int main() {
	Adres moj_adres("Malczewskiego", 201, "Gdansk");
	cout << "Ulica: " << moj_adres.getStreet() << endl;
	cout << "Numer domu: " << moj_adres.getHouse_number() << endl;
	cout << "Miasto: " << moj_adres.getCity() << endl;
	moj_adres.setCity("Warszawa");
	moj_adres.setStreet("Garibaldiego");
	moj_adres.setHouse_number(21);
	cout << "Ulica: " << moj_adres.getStreet() << endl;
	cout << "Numer domu: " << moj_adres.getHouse_number() << endl;
	cout << "Miasto: " << moj_adres.getCity() << endl;

	cout << "\n------------------------------\n";

	Klient moj_klient("Jakub", "Cieslak", moj_adres);

	cout << "Imie klienta: " << moj_klient.getName() << endl;
	cout << "Nazwisko klienta: " << moj_klient.getSurname() << endl;
	cout << "Miasto Klienta: " << moj_klient.getAdres().getCity() << endl;
	moj_klient.setAdres(Adres("Jasna", 10, "Rzeszow"));
	cout << "Klient sie przeprowadzil, jego nowe miasto zamieszkania to: \n";
	cout << "Miasto Klienta: " << moj_klient.getAdres().getCity() << endl;

	cout << "\n------------------------------\n";

	Adres adresbanku("Bankowa", 1, "Warszawa");
	Bank mbank("Mbank", adresbanku);
	cout << "Nazwa banku: " << mbank.getName() << endl;
	cout << "Siedziba placowki banku: \n" << "Ulica: " << mbank.getAdres().getStreet() << endl;
	cout << "Numer budynku: " << mbank.getAdres().getHouse_number() << endl;
	cout << "Miasto: " << mbank.getAdres().getCity() << endl;

	mbank.setAdres(moj_adres);
	cout << "Informujemy, ze bank zostal przeniesiony! Prosimy zapoznac sie z nowa lokalizacja placowki \n";
	cout << "Siedziba placowki banku: \n" << "Ulica: " << mbank.getAdres().getStreet() << endl;
	cout << "Numer budynku: " << mbank.getAdres().getHouse_number() << endl;
	cout << "Miasto: " << mbank.getAdres().getCity() << endl;

	cout << "Mbank polaczyl sie z Santander Bank tworzac MsantBank\n";
	mbank.setName("MsantBank");
	cout << "Nazwa banku: " << mbank.getName() << endl;

	cout << "\n------------------------------\n";

	Rachunek moj_rachunek(10234, moj_klient, 1000, mbank);
	cout << "Moj numer konta: " << moj_rachunek.getNumber() << endl;
	cout << "Moje saldo: " << moj_rachunek.getSaldo() << endl;
	cout << "Moje imie i nazwisko: " << moj_rachunek.getKlient().getName() << " " << moj_rachunek.getKlient().getSurname() << endl;
	moj_klient.setSurname("Korbacz");
	moj_rachunek.setKlient(moj_klient);
	cout << "Przyjelismy twoj wniosek o zmiane nazwiska, twoje nowe nazwisko to: " << moj_rachunek.getKlient().getSurname() << endl;
	cout << "Przyjelismy twoj przelew, odjelismy z twojego konta 500 zl. \n";
	int przelew = 500;
	moj_rachunek.setSaldo(moj_rachunek.getSaldo() - przelew);
	cout << "Twoj aktualny stan konta to: " << moj_rachunek.getSaldo() << " zl." <<endl;
	moj_rachunek.setNumber(1234);
	cout << "W zwiazku z atakiem na nasza baze danych musielismy zmienic Twoj dotychczasowy nr konta, jest on nastepujacy: " << moj_rachunek.getNumber() << endl;
	Klient drugi_klient("Lukasz", "Cieslak", moj_adres);
	Rachunek drugi_rachunek(1111, drugi_klient, 0, "PKO", Adres());
	cout << "Moj numer konta: " << drugi_rachunek.getNumber() << endl;
	cout << "Moje saldo: " << drugi_rachunek.getSaldo() << endl;
	cout << "Moje imie i nazwisko: " << drugi_rachunek.getKlient().getName() << " " << drugi_rachunek.getKlient().getSurname() << endl;

	cout << "Przyjelismy przelew od " << moj_rachunek.getKlient().getName() << " " << moj_rachunek.getKlient().getSurname() << " na Twoje konto\n";
	drugi_rachunek.setSaldo(drugi_rachunek.getSaldo() + przelew);
	cout << "Twoj aktualny stan konta to: " << moj_rachunek.getSaldo() << " zl." << endl;
	system("pause");
		return 0;
}