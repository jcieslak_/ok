#include "Klient.h"

Klient::Klient()
{
	cout << "Dziala konstruktor klasy Klient bez arg.\n";
	Name = "Jan";
	Surname = "Kowalski";
}

Klient::Klient(string n, string sn, Adres adres) : Name(n), Surname(sn), adres(adres) 
{
	cout << "Dziala kontsruktor klasy Klient z arg.\n";
}

void Klient::setName(string n) { Name = n; }
void Klient::setSurname(string sn) { Surname = sn; }
void Klient::setAdres(Adres ad) { adres = ad; }

string Klient::getName() { return Name; }
string Klient::getSurname() { return Surname; }
Adres Klient::getAdres() { return adres; }