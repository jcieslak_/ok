#pragma once
#include"Klient.h"
#include"Bank.h"

class Rachunek : public Bank 
{
	unsigned int Number;
	Klient klient;
	float Saldo; // niestety c++ nie ma nieznakowanych liczb zmiennoprzecinkowych :(
public:
	Rachunek();
	Rachunek(int numer, Klient klient, float saldo, Bank bank);
	Rachunek(int numer, Klient klient, float saldo, string name, Adres adres);

	void setNumber(unsigned int);
	void setKlient(Klient);
	void setSaldo(float);

	unsigned int getNumber();
	Klient getKlient();
	float getSaldo();


};