#pragma once
#include<iostream>
using namespace std;

class Adres {
	string street;
	int house_number;
	string city;
public:
	Adres();
	Adres(string street, int house_number, string city);

	void setStreet(string);
	void setHouse_number(int);
	void setCity(string);

	string getStreet();
	int getHouse_number();
	string getCity();

};
