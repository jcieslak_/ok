#include "Bank.h"

Bank::Bank()
{
	cout << "Dziala konstruktor klasy Bank bez arg. \n";
	Name = "mbank";
	adres = Adres();
}

Bank::Bank(string n, Adres ad) : Name(n), adres(ad)
{
	cout << "Dziala konstruktor klasy Bank z arg. \n";
}

void Bank::setName(string n) { Name = n; }
void Bank::setAdres(Adres ad) { adres = ad; }

string Bank::getName() { return Name; }
Adres Bank::getAdres() { return adres; }