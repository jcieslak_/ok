#include "Rachunek.h"
Rachunek::Rachunek()
{
	cout << "Dziala konstruktor klasy Rachunek bez arg. \n";
		Number = 1;
		klient = Klient();
		Saldo = 0;
}

Rachunek::Rachunek(int n, Klient k, float s, Bank bank) : Number(n), klient(k), Saldo(s)
{
	cout << "Dziala konstruktor klasy Rachunek z 4 arg. \n";
}

Rachunek::Rachunek(int n, Klient k, float s, string N, Adres ad) : Number(n), klient(k), Saldo(s), Bank(N, ad)
{
	cout << "Dziala konstruktor klasy Rachunek z 5 arg. \n";
}

void Rachunek::setNumber(unsigned int n) { Number = n; }
void Rachunek::setKlient(Klient kl) { klient = kl; }
void Rachunek::setSaldo(float sald) { Saldo = sald; }

unsigned int Rachunek::getNumber() { return Number; }
Klient Rachunek::getKlient() { return klient; }
float Rachunek::getSaldo() { return Saldo; }