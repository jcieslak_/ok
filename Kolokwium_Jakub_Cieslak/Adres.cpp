#include "Adres.h"

Adres::Adres()
{
	cout << "Dziala konstruktor klasy Adres bez arg.\n";
	street = "Narutowicza";
	house_number = 11;
	city = "Gdansk";
}

Adres::Adres(string st, int h_n, string ci) :
	street(st), house_number(h_n), city(ci)
{
	cout << "Dziala konstruktor klasy Adres z arg.\n";
}


void Adres::setStreet(string s) { street = s; }
void Adres::setHouse_number(int hn) { house_number = hn; }
void Adres::setCity(string c) { city = c; }

string Adres::getStreet() { return street; }
int Adres::getHouse_number() { return house_number; }
string Adres::getCity() { return city; }