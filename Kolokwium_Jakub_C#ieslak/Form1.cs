﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kolokwium_Jakub_Cieslak
{
    public partial class Form_quiz : Form
    {
        public Form_quiz()
        {
            InitializeComponent();
        }

        private void button_odpowiedz_Click(object sender, EventArgs e)
        {
            string[] Wyniki = new string[5] { "1. zle", "2. zle", "3. zle", "4. zle", "5. ilosc punktow: 0" } ;
            double wynik = 0;
            if (textBox_pytanie1.Text == "1991")
            {
                wynik += 1;
                Wyniki[0] = "1. dobrze";
            }
            if (radioButton_odpowiedz22.Checked == true)
            {
                wynik += 1;
                Wyniki[1] = "2. dobrze";
            }
            if (comboBox_pytanie3.SelectedItem.ToString() == "Pellowski")
            {
                wynik += 1;
                Wyniki[2] = "3. dobrze";
            }
            if (dateTimePicker_pytanie4.Value.ToShortDateString() == "14.02.2001")
            {
                wynik += 1;
                Wyniki[3] = "4. dobrze";
            }
            double wynik_listbox = 0;
            foreach (int indexChecked in checkedListBox_pytanie5.CheckedIndices)
            { //Za zestaw poprawnych odpowiedzi uzytkownik otrzymuje 1 pkt, jesli jednak strzeli zle, dostaje kare w postaci utraty 0.5 pkta
                if (indexChecked == 0)
                {
                    wynik_listbox += 0.5;
                }
                if (indexChecked == 1)
                {
                    wynik_listbox -= 0.5;
                }
                if (indexChecked == 2)
                {
                    wynik_listbox += 0.5;
                }
                if (indexChecked == 3)
                {
                    wynik_listbox -= 0.5;
                }
            }
            Wyniki[4] = "5. ilosc punktow: " + wynik_listbox.ToString();
            wynik += wynik_listbox;
            if (wynik < 0)
            {
                wynik = 0;
            }
            MessageBox.Show(wynik.ToString() + "/5" + "\n\n\n" + "Odpowiedzi: \n" + string.Join("\n", Wyniki), "Wynik Quizu: ", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }


        private void Form_quiz_Load(object sender, EventArgs e)
        {
            comboBox_pytanie3.SelectedIndex = 0;
        }
    }
}
