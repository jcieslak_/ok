﻿namespace Kolokwium_Jakub_Cieslak
{
    partial class Form_quiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_pytanie = new System.Windows.Forms.GroupBox();
            this.label_pytanie5_uzupelnienie = new System.Windows.Forms.Label();
            this.label_pytanie5 = new System.Windows.Forms.Label();
            this.checkedListBox_pytanie5 = new System.Windows.Forms.CheckedListBox();
            this.label_pytanie4 = new System.Windows.Forms.Label();
            this.dateTimePicker_pytanie4 = new System.Windows.Forms.DateTimePicker();
            this.label_pytanie3 = new System.Windows.Forms.Label();
            this.comboBox_pytanie3 = new System.Windows.Forms.ComboBox();
            this.groupBox_pytanie2 = new System.Windows.Forms.GroupBox();
            this.radioButton_odpowiedz23 = new System.Windows.Forms.RadioButton();
            this.radioButton_odpowiedz22 = new System.Windows.Forms.RadioButton();
            this.radioButton_odpowiedz21 = new System.Windows.Forms.RadioButton();
            this.label_pytanie2 = new System.Windows.Forms.Label();
            this.label_pytanie1 = new System.Windows.Forms.Label();
            this.textBox_pytanie1 = new System.Windows.Forms.TextBox();
            this.button_odpowiedz = new System.Windows.Forms.Button();
            this.groupBox_pytanie.SuspendLayout();
            this.groupBox_pytanie2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_pytanie
            // 
            this.groupBox_pytanie.Controls.Add(this.label_pytanie5_uzupelnienie);
            this.groupBox_pytanie.Controls.Add(this.label_pytanie5);
            this.groupBox_pytanie.Controls.Add(this.checkedListBox_pytanie5);
            this.groupBox_pytanie.Controls.Add(this.label_pytanie4);
            this.groupBox_pytanie.Controls.Add(this.dateTimePicker_pytanie4);
            this.groupBox_pytanie.Controls.Add(this.label_pytanie3);
            this.groupBox_pytanie.Controls.Add(this.comboBox_pytanie3);
            this.groupBox_pytanie.Controls.Add(this.groupBox_pytanie2);
            this.groupBox_pytanie.Controls.Add(this.label_pytanie1);
            this.groupBox_pytanie.Controls.Add(this.textBox_pytanie1);
            this.groupBox_pytanie.Location = new System.Drawing.Point(12, 12);
            this.groupBox_pytanie.Name = "groupBox_pytanie";
            this.groupBox_pytanie.Size = new System.Drawing.Size(404, 422);
            this.groupBox_pytanie.TabIndex = 0;
            this.groupBox_pytanie.TabStop = false;
            this.groupBox_pytanie.Text = "Pytania";
            // 
            // label_pytanie5_uzupelnienie
            // 
            this.label_pytanie5_uzupelnienie.AutoSize = true;
            this.label_pytanie5_uzupelnienie.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_pytanie5_uzupelnienie.ForeColor = System.Drawing.Color.Red;
            this.label_pytanie5_uzupelnienie.Location = new System.Drawing.Point(1, 290);
            this.label_pytanie5_uzupelnienie.Name = "label_pytanie5_uzupelnienie";
            this.label_pytanie5_uzupelnienie.Size = new System.Drawing.Size(396, 13);
            this.label_pytanie5_uzupelnienie.TabIndex = 9;
            this.label_pytanie5_uzupelnienie.Text = "Uwaga! Za niepoprawne odpowiedzi przyznawane są ujemne punkty.";
            // 
            // label_pytanie5
            // 
            this.label_pytanie5.AutoSize = true;
            this.label_pytanie5.Location = new System.Drawing.Point(1, 272);
            this.label_pytanie5.Name = "label_pytanie5";
            this.label_pytanie5.Size = new System.Drawing.Size(268, 13);
            this.label_pytanie5.TabIndex = 8;
            this.label_pytanie5.Text = "Które z wymienionych osób są w zarządzie spółki LPP?";
            // 
            // checkedListBox_pytanie5
            // 
            this.checkedListBox_pytanie5.CheckOnClick = true;
            this.checkedListBox_pytanie5.FormattingEnabled = true;
            this.checkedListBox_pytanie5.Items.AddRange(new object[] {
            "Marek Piechocki",
            "Krzysztof Ibisz",
            "Sławomir Łoboda",
            "Maryla Rodowicz"});
            this.checkedListBox_pytanie5.Location = new System.Drawing.Point(4, 306);
            this.checkedListBox_pytanie5.Name = "checkedListBox_pytanie5";
            this.checkedListBox_pytanie5.Size = new System.Drawing.Size(120, 94);
            this.checkedListBox_pytanie5.TabIndex = 7;
            // 
            // label_pytanie4
            // 
            this.label_pytanie4.AutoSize = true;
            this.label_pytanie4.Location = new System.Drawing.Point(7, 227);
            this.label_pytanie4.Name = "label_pytanie4";
            this.label_pytanie4.Size = new System.Drawing.Size(279, 13);
            this.label_pytanie4.TabIndex = 6;
            this.label_pytanie4.Text = "Kiedy LPP zostało wpisane do Rejestru Przedsiębiorców?";
            // 
            // dateTimePicker_pytanie4
            // 
            this.dateTimePicker_pytanie4.Location = new System.Drawing.Point(7, 249);
            this.dateTimePicker_pytanie4.Name = "dateTimePicker_pytanie4";
            this.dateTimePicker_pytanie4.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_pytanie4.TabIndex = 5;
            // 
            // label_pytanie3
            // 
            this.label_pytanie3.AutoSize = true;
            this.label_pytanie3.Location = new System.Drawing.Point(7, 180);
            this.label_pytanie3.Name = "label_pytanie3";
            this.label_pytanie3.Size = new System.Drawing.Size(194, 13);
            this.label_pytanie3.TabIndex = 4;
            this.label_pytanie3.Text = "Która marka NIE należy do spółki LPP?";
            // 
            // comboBox_pytanie3
            // 
            this.comboBox_pytanie3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_pytanie3.FormattingEnabled = true;
            this.comboBox_pytanie3.Items.AddRange(new object[] {
            "Cropp",
            "Reserved",
            "Pellowski",
            "House",
            "Mohito"});
            this.comboBox_pytanie3.Location = new System.Drawing.Point(6, 199);
            this.comboBox_pytanie3.Name = "comboBox_pytanie3";
            this.comboBox_pytanie3.Size = new System.Drawing.Size(121, 21);
            this.comboBox_pytanie3.TabIndex = 3;
            // 
            // groupBox_pytanie2
            // 
            this.groupBox_pytanie2.Controls.Add(this.radioButton_odpowiedz23);
            this.groupBox_pytanie2.Controls.Add(this.radioButton_odpowiedz22);
            this.groupBox_pytanie2.Controls.Add(this.radioButton_odpowiedz21);
            this.groupBox_pytanie2.Controls.Add(this.label_pytanie2);
            this.groupBox_pytanie2.Location = new System.Drawing.Point(6, 62);
            this.groupBox_pytanie2.Name = "groupBox_pytanie2";
            this.groupBox_pytanie2.Size = new System.Drawing.Size(195, 113);
            this.groupBox_pytanie2.TabIndex = 2;
            this.groupBox_pytanie2.TabStop = false;
            // 
            // radioButton_odpowiedz23
            // 
            this.radioButton_odpowiedz23.AutoSize = true;
            this.radioButton_odpowiedz23.Location = new System.Drawing.Point(7, 85);
            this.radioButton_odpowiedz23.Name = "radioButton_odpowiedz23";
            this.radioButton_odpowiedz23.Size = new System.Drawing.Size(79, 17);
            this.radioButton_odpowiedz23.TabIndex = 3;
            this.radioButton_odpowiedz23.TabStop = true;
            this.radioButton_odpowiedz23.Text = "Dzierzążnia";
            this.radioButton_odpowiedz23.UseVisualStyleBackColor = true;
            // 
            // radioButton_odpowiedz22
            // 
            this.radioButton_odpowiedz22.AutoSize = true;
            this.radioButton_odpowiedz22.Location = new System.Drawing.Point(7, 61);
            this.radioButton_odpowiedz22.Name = "radioButton_odpowiedz22";
            this.radioButton_odpowiedz22.Size = new System.Drawing.Size(62, 17);
            this.radioButton_odpowiedz22.TabIndex = 2;
            this.radioButton_odpowiedz22.TabStop = true;
            this.radioButton_odpowiedz22.Text = "Gdańsk";
            this.radioButton_odpowiedz22.UseVisualStyleBackColor = true;
            // 
            // radioButton_odpowiedz21
            // 
            this.radioButton_odpowiedz21.AutoSize = true;
            this.radioButton_odpowiedz21.Location = new System.Drawing.Point(7, 37);
            this.radioButton_odpowiedz21.Name = "radioButton_odpowiedz21";
            this.radioButton_odpowiedz21.Size = new System.Drawing.Size(75, 17);
            this.radioButton_odpowiedz21.TabIndex = 1;
            this.radioButton_odpowiedz21.TabStop = true;
            this.radioButton_odpowiedz21.Text = "Warszawa";
            this.radioButton_odpowiedz21.UseVisualStyleBackColor = true;
            // 
            // label_pytanie2
            // 
            this.label_pytanie2.AutoSize = true;
            this.label_pytanie2.Location = new System.Drawing.Point(7, 20);
            this.label_pytanie2.Name = "label_pytanie2";
            this.label_pytanie2.Size = new System.Drawing.Size(172, 13);
            this.label_pytanie2.TabIndex = 0;
            this.label_pytanie2.Text = "W jakim mieście jest centrala LPP?";
            // 
            // label_pytanie1
            // 
            this.label_pytanie1.AutoSize = true;
            this.label_pytanie1.Location = new System.Drawing.Point(7, 20);
            this.label_pytanie1.Name = "label_pytanie1";
            this.label_pytanie1.Size = new System.Drawing.Size(190, 13);
            this.label_pytanie1.TabIndex = 1;
            this.label_pytanie1.Text = "W którym roku zostało założone LPP?";
            // 
            // textBox_pytanie1
            // 
            this.textBox_pytanie1.Location = new System.Drawing.Point(6, 36);
            this.textBox_pytanie1.Name = "textBox_pytanie1";
            this.textBox_pytanie1.Size = new System.Drawing.Size(100, 20);
            this.textBox_pytanie1.TabIndex = 0;
            // 
            // button_odpowiedz
            // 
            this.button_odpowiedz.Location = new System.Drawing.Point(12, 440);
            this.button_odpowiedz.Name = "button_odpowiedz";
            this.button_odpowiedz.Size = new System.Drawing.Size(404, 23);
            this.button_odpowiedz.TabIndex = 1;
            this.button_odpowiedz.Text = "Sprawdź odpowiedzi!";
            this.button_odpowiedz.UseVisualStyleBackColor = true;
            this.button_odpowiedz.Click += new System.EventHandler(this.button_odpowiedz_Click);
            // 
            // Form_quiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 493);
            this.Controls.Add(this.button_odpowiedz);
            this.Controls.Add(this.groupBox_pytanie);
            this.Name = "Form_quiz";
            this.Text = "Quiz";
            this.Load += new System.EventHandler(this.Form_quiz_Load);
            this.groupBox_pytanie.ResumeLayout(false);
            this.groupBox_pytanie.PerformLayout();
            this.groupBox_pytanie2.ResumeLayout(false);
            this.groupBox_pytanie2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_pytanie;
        private System.Windows.Forms.CheckedListBox checkedListBox_pytanie5;
        private System.Windows.Forms.Label label_pytanie4;
        private System.Windows.Forms.DateTimePicker dateTimePicker_pytanie4;
        private System.Windows.Forms.Label label_pytanie3;
        private System.Windows.Forms.ComboBox comboBox_pytanie3;
        private System.Windows.Forms.GroupBox groupBox_pytanie2;
        private System.Windows.Forms.RadioButton radioButton_odpowiedz23;
        private System.Windows.Forms.RadioButton radioButton_odpowiedz22;
        private System.Windows.Forms.RadioButton radioButton_odpowiedz21;
        private System.Windows.Forms.Label label_pytanie2;
        private System.Windows.Forms.Label label_pytanie1;
        private System.Windows.Forms.TextBox textBox_pytanie1;
        private System.Windows.Forms.Label label_pytanie5;
        private System.Windows.Forms.Button button_odpowiedz;
        private System.Windows.Forms.Label label_pytanie5_uzupelnienie;
    }
}

