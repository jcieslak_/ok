#pragma once
#include<iostream>
#include"Date.h"

using namespace std;

class Person{
	string name;
	string surname;
	Date birthday;
public:
	Person();
	Person(string name, string surname, Date birthday);

	void setName(string);
	void setSurname(string);
	void setDate(Date);

	string getName();
	string getSurname();
	Date getDate();
};