#pragma once
#include<iostream>
#include "Date.h"

using namespace std;

Date::Date()
{
	cout << "Dziala konstruktor klasy Date bez arg.\n";
	day = 1;
	month = 1;
	year = 1900;
}

Date::Date(int d, int m, int y):day(d), month(m), year(y)
{
	cout << "Dziala konstruktor klasy Date z arg.\n";
}

void Date::setDay(int d) { day = d; }
void Date::setMonth(int m) { month = m; }
void Date::setYear(int y) { year = y; }

int Date::getDay(){ return day; }
int Date::getMonth() { return month; }
int Date::getYear() { return year; }

