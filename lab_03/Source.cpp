#include<iostream>
#include<string>
#include"Date.h"
#include"Person.h"
#include"Address.h"

using namespace std;

int main() {

	Date my_date(11,3,2020);

	cout << "Data: " << my_date.getYear() <<
		"-" << my_date.getMonth() << "-" <<
		my_date.getDay() << endl;

	cout << "\n------------------------------\n";

	Person my_person;

	cout << "Imie i nazwisko: " << my_person.getName() <<
		" " << my_person.getSurname() << endl;

	cout << "Data my_person: " << my_person.getDate().getYear() <<
		"-" << my_person.getDate().getMonth() << "-" <<
		my_person.getDate().getDay() << endl;
	
	cout << "\n------------------------------\n";

	int tmp;
	cout << "Podaj rok\n";
	cin >> tmp;

	Person new_person("Edward", "Trzeci", Date(45,30,tmp));

	cout << "Imie i nazwisko new_person: " << new_person.getName() <<
		" " << new_person.getSurname() << endl;

	cout << "Data new_person: " << new_person.getDate().getYear() <<
		"-" << new_person.getDate().getMonth() << "-" <<
		new_person.getDate().getDay() << endl;

	cout << "\n------------------------------\n";

	Address my_address;

	cout << "Kod pocztowy: " << my_address.getPost_code()[0] << "-" << 
		my_address.getPost_code()[1] << endl;

	cout << "\n------------------------------\n";

	int my_post_code[] = { 11, 135 };

	my_address.setPost_code(my_post_code);

	cout << "Kod pocztowy: " << my_address.getPost_code()[0] << "-" <<
		my_address.getPost_code()[1] << endl;

	cout << "\n------------------------------\n";

	Address new_address("Piwna", 3, 5, my_post_code, "Gdansk");

	cout << "Ulica: " << new_address.getStreet() << endl;
	cout << "Numer domu: " << new_address.getHouse_number() << endl;
	cout << "Numer mieszkania " << new_address.getFlat_number() << endl;
	cout << "Kod pocztowy: " << new_address.getPost_code()[0] << "-" <<
		new_address.getPost_code()[1] << endl;
	cout << "Miasto: " << new_address.getCity() << endl;

	system("pause");
	return 0;
}