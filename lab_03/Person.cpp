#include "Person.h"

Person::Person()
{
	cout << "Dziala konstruktor klasy Person bez arg.\n";
	name = "Jane";
	surname = "Doe";
}

Person::Person(string n, string sn, Date bd): name(n), surname(sn), birthday(bd)
{
	cout << "Dziala konstruktor klasy Person z arg.\n";
}

void Person::setName(string n) { name = n; }
void Person::setSurname(string sn) { surname = sn; }
void Person::setDate(Date bd) { birthday = bd; }

string Person::getName() { return name; }
string Person::getSurname() { return surname; }
Date Person::getDate() { return birthday; }