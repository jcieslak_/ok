#include "Address.h"

Address::Address()
{
	cout << "Dziala konstruktor klasy Address bez arg.\n";
	street = "Narutowicza";
	house_number = 11;
	flat_number = 12;
	post_code[0] = 80;
	post_code[1] = 233;
	city = "Gdansk";
}

Address::Address(string st, int h_n, int f_n, int *p_c, string ci):
	street(st), house_number(h_n), flat_number(f_n), city(ci)
{
	cout << "Dziala konstruktor klasy Address z arg.\n";
	for (int i = 0; i < size(post_code); i++) {
		post_code[i] = p_c[i];
	}
}

void Address::setStreet(string s) { street = s; }
void Address::setHouse_number(int hn) { house_number = hn; }
void Address::setFlat_number(int fn) { flat_number = fn; }

void Address::setPost_code(int *pc) {
	for (int i = 0; i < size(post_code); i++) {
		post_code[i] = pc[i];
	}
}
void Address::setCity(string c) { city = c; }

string Address::getStreet() { return street; }
int Address::getHouse_number() { return house_number; }
int Address::getFlat_number() { return flat_number; }
int* Address::getPost_code() { return post_code; }
string Address::getCity() { return city; }