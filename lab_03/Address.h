#pragma once
#include<iostream>

using namespace std;

class Address {
	string street;
	int house_number;
	int flat_number;
	int post_code[2];
	string city;
public:
	Address();
	Address(string street, int house_number, int flat_number,
		int* post_code, string city);

	void setStreet(string);
	void setHouse_number(int);
	void setFlat_number(int);
	void setPost_code(int*);
	void setCity(string);

	string getStreet();
	int getHouse_number();
	int getFlat_number();
	int* getPost_code();
	string getCity();

};