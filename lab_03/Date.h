#pragma once


class Date {
	int day;
	int month;
	int year;

public:
	Date();
	Date(int day, int month, int year);

	void setDay(int);
	void setMonth(int);
	void setYear(int);

	int getDay();
	int getMonth();
	int getYear();
};