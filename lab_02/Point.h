#pragma once

class Point{
	float x;
	float y;
public:
	Point();
	Point(float, float);
	Point(float, float, int);

	void setX(float);
	void setY(float);

	float getX();
	float getY();

	float distance();
};