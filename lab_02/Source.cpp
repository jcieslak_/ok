#include<iostream>
#include"Point.h"

using namespace std;

int main() {

	Point my_point;

	cout << "Rozmiar obiektu my_point wynosi " << sizeof(my_point) << endl;

	my_point.setX(5.5);

	cout << "Wsp. x obiektu my_point " << my_point.getX() << endl;
	cout << "Wsp. y obiektu my_point " << my_point.getY() << endl;

	float tmp;
	cout << "Podaj wsp. x\n";
	cin >> tmp;
	my_point.setX(tmp);

	cout << "Podaj wsp. y\n";
	cin >> tmp;
	my_point.setY(tmp);

	cout << "Odleglosc punktu my_point od pocz. ukl. wsp. " << my_point.distance() << endl;

	cout << "\n-----------------------------\n";

	Point new_point(1.111, 2.222);

	cout << "Wsp. x obiektu new_point " << new_point.getX() << endl;
	cout << "Wsp. y obiektu new_point " << new_point.getY() << endl;

	float tmpX;
	cout << "Podaj wsp. x\n";
	cin >> tmpX;

	float tmpY;
	cout << "Podaj wsp. y\n";
	cin >> tmpY;

	cout << "Odleglosc punktu my_point od pocz. ukl. wsp. " << Point(tmpX, tmpY).distance() << endl;

	cout << "\n-----------------------------\n";

	cout << "Podaj wsp. x\n";
	cin >> tmpX;

	cout << "Podaj wsp. y\n";
	cin >> tmpY;

	int cw;
	cout << "Podaj numer cwiartki\n";
	cin >> cw;

	Point super_point(tmpX, tmpY, cw);

	cout << "Wsp. x obiektu super_point " << super_point.getX() << endl;
	cout << "Wsp. y obiektu super_point " << super_point.getY() << endl;

	system("pause");
	return 0;
}