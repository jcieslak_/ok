#pragma once
#include<iostream>
#include"Point.h"

using namespace std;

Point::Point() {
	cout << "Dziala konstruktor klasy Point bez arg.\n";
	x = 0.0;
	y = 0.0;
}

Point::Point(float a, float b) : x(a), y(b) {
	cout << "Dziala konstruktor klasy Point z arg.\n";
}

Point::Point(float a, float b, int c) {
	if (c < 1 || c > 4) {
		cout << "Podano niepoprawny numer cwiartki\n";
		cout << "Numer cwiartki ustawiono na wartosc domyslna, czyli 1\n";
		c = 1;
	}

	if (c == 1 && a >= 0.0 && b >= 0.0) {
		cout << "Dane poprawne\n";
		x = a;
		y = b;
	}
	else if (c == 2 && a < 0.0 && b >= 0.0) {
		cout << "Dane poprawne\n";
		x = a;
		y = b;
	}
	else if (c == 3 && a < 0.0 && b < 0.0) {
		cout << "Dane poprawne\n";
		x = a;
		y = b;
	}
	else if (c == 4 && a >= 0.0 && b < 0.0) {
		cout << "Dane poprawne\n";
		x = a;
		y = b;
	}
	else
	{
		cout << "Dane niepoprawne\n";
		cout << "Wprowadzam wartosci domyslne\n";
		x = 0.0;
		y = 0.0;
	}
}

void Point::setX(float a) { x = a; }
void Point::setY(float b) { y = b; }

float Point::getX() { return x; }
float Point::getY() { return y; }

float Point::distance() { return sqrtf(x*x + y*y); }